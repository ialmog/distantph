require 'test_helper'

class HealingsControllerTest < ActionController::TestCase
  setup do
    @healing = healings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:healings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create healing" do
    assert_difference('Healing.count') do
      post :create, healing: { comment: @healing.comment, price: @healing.price, session_time: @healing.session_time }
    end

    assert_redirected_to healing_path(assigns(:healing))
  end

  test "should show healing" do
    get :show, id: @healing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @healing
    assert_response :success
  end

  test "should update healing" do
    patch :update, id: @healing, healing: { comment: @healing.comment, price: @healing.price, session_time: @healing.session_time }
    assert_redirected_to healing_path(assigns(:healing))
  end

  test "should destroy healing" do
    assert_difference('Healing.count', -1) do
      delete :destroy, id: @healing
    end

    assert_redirected_to healings_path
  end
end

class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :full_name
      t.string :password
      t.string :email
      t.integer :age
      t.string :location
      t.text :bio
      t.string :phone

      t.timestamps
    end
  end
end

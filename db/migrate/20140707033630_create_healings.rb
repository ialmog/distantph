class CreateHealings < ActiveRecord::Migration
  def change
    create_table :healings do |t|
      t.decimal :price
      t.datetime :session_time
      t.text :comment

      t.timestamps
    end
  end
end

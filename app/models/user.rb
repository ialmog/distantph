class User < ActiveRecord::Base
  
  has_many :healings
  has_many :healers, through: :healings
end

class HealingsController < ApplicationController
  before_action :set_healing, only: [:show, :edit, :update, :destroy]

  # GET /healings
  # GET /healings.json
  def index
    @healings = Healing.all
  end

  # GET /healings/1
  # GET /healings/1.json
  def show
  end

  # GET /healings/new
  def new
    @healing = Healing.new
  end

  # GET /healings/1/edit
  def edit
  end

  # POST /healings
  # POST /healings.json
  def create
    @healing = Healing.new(healing_params)

    respond_to do |format|
      if @healing.save
        format.html { redirect_to @healing, notice: 'Healing was successfully created.' }
        format.json { render :show, status: :created, location: @healing }
      else
        format.html { render :new }
        format.json { render json: @healing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /healings/1
  # PATCH/PUT /healings/1.json
  def update
    respond_to do |format|
      if @healing.update(healing_params)
        format.html { redirect_to @healing, notice: 'Healing was successfully updated.' }
        format.json { render :show, status: :ok, location: @healing }
      else
        format.html { render :edit }
        format.json { render json: @healing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /healings/1
  # DELETE /healings/1.json
  def destroy
    @healing.destroy
    respond_to do |format|
      format.html { redirect_to healings_url, notice: 'Healing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_healing
      @healing = Healing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def healing_params
      params.require(:healing).permit(:price, :session_time, :comment)
    end
end

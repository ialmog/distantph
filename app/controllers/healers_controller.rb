class HealersController < ApplicationController
  before_action :set_healer, only: [:show, :edit, :update, :destroy]

  # GET /healers
  # GET /healers.json
  def index
    @healers = Healer.all
  end

  # GET /healers/1
  # GET /healers/1.json
  def show
  end

  # GET /healers/new
  def new
    @healer = Healer.new
  end

  # GET /healers/1/edit
  def edit
  end

  # POST /healers
  # POST /healers.json
  def create
    @healer = Healer.new(healer_params)

    respond_to do |format|
      if @healer.save
        format.html { redirect_to @healer, notice: 'Healer was successfully created.' }
        format.json { render :show, status: :created, location: @healer }
      else
        format.html { render :new }
        format.json { render json: @healer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /healers/1
  # PATCH/PUT /healers/1.json
  def update
    respond_to do |format|
      if @healer.update(healer_params)
        format.html { redirect_to @healer, notice: 'Healer was successfully updated.' }
        format.json { render :show, status: :ok, location: @healer }
      else
        format.html { render :edit }
        format.json { render json: @healer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /healers/1
  # DELETE /healers/1.json
  def destroy
    @healer.destroy
    respond_to do |format|
      format.html { redirect_to healers_url, notice: 'Healer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_healer
      @healer = Healer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def healer_params
      params.require(:healer).permit(:certlevel)
    end
end

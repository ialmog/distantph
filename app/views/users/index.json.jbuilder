json.array!(@users) do |user|
  json.extract! user, :id, :username, :full_name, :password, :email, :age, :location, :bio, :phone
  json.url user_url(user, format: :json)
end

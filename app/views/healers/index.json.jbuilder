json.array!(@healers) do |healer|
  json.extract! healer, :id, :certlevel
  json.url healer_url(healer, format: :json)
end

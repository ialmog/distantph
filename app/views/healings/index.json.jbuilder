json.array!(@healings) do |healing|
  json.extract! healing, :id, :price, :session_time, :comment
  json.url healing_url(healing, format: :json)
end
